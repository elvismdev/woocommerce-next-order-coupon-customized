��          �      l      �     �     �            $   )     N  $   ^     �     �  
   �     �     �     �  *   �  9     B   H     �  �   �  S   #  `   w    �     �     �       !     %   4     Z  *   s     �     �     �     �     �     �  7     =   E  J   �     �  �   �  m   �	  l   �	                                                             
         	                            After order table Before order table Coupon Coupon is non-transferrable Coupon length (number of characters) Coupon settings Custom location (define in template) Email Email settings Email text Enable Next Order Coupon Expires in (days) Minimum order amount Minimum order amount for coupon generation Number of days the coupon code is valid after generation. Only works with the email address of the customer placed the order Text placement Text to be included in order confirmation emails.<br /><code>%1$s</code> = coupon code<br /><code>%2$s</code> = coupon amount/percentage Use coupon code <strong>%1$s</strong> on your next visit and receive %2$s discount! WooCommerce Next Order Coupon requires <a href="%s">WooCommerce</a> to be installed & activated! Project-Id-Version: WooCommerce Automatic Coupon Generator
POT-Creation-Date: 2013-11-10 12:21+0100
PO-Revision-Date: 2013-11-10 12:22+0100
Last-Translator: Ewout Fernhout <chocolade@extrapuur.nl>
Language-Team: WP Overnight <support@wpovernight.com>
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.5
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e
X-Poedit-Basepath: ../
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Na bestellingstabel Vóór bestellingstabel Kortingsbon Kortingsbon is niet overdraagbaar Kortingsbon lengte (aantal karakters) Kortingsbon instellingen Aangepaste locatie (definieer in template) Email Email instellingen Email tekst Next Order Coupon activeren Verloopt in (dagen) Minimum bestellingsbedrag Minimum bedrag waarbij een kortingsbon wordt aangemaakt Aantal dagen dat de kortingsbon geldig is na de aanmaakdatum. Werkt alleen met het email adres van de klant die de order heeft geplaatst Plaatsing tekst Tekst die wordt toegevoegd aan de bevestigingsmail voor de klant.<br /><code>%1$s</code> = Kortingsbon<br /><code>%2$s</code> = Kortingsbon hoeveelheid/percentage Gebruik de kortingsbon <strong>%1$s</strong> bij uw volgende bezoek en ontvang %2$s korting op uw bestelling! WooCommerce Next Order Coupon werkt alleen als <a href="%s">WooCommerce</a> is geïnstalleerd & geactiveerd! 