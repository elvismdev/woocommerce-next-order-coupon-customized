<?php
class WC_Next_Order_Coupon_Settings {

	public $options_page_hook;

	public function __construct() {
		add_action( 'admin_menu', array( &$this, 'menu' ) ); // Add menu.
		add_action( 'admin_init', array( &$this, 'init_settings' ) ); // Registers settings
		add_filter( 'plugin_action_links_'.WooCommerce_Next_Order_Coupon::$plugin_basename, array( &$this, 'wcnoc_add_settings_link' ) );
		add_action( 'admin_enqueue_scripts', array( &$this, 'load_scripts_styles' ) ); // Load scripts
	}

	public function menu() {
		if (class_exists('WPOvernight_Core')) {
			$parent_slug = 'wpo-core-menu';
		} else {
			$parent_slug = 'woocommerce';
		}

		$this->options_page_hook = add_submenu_page(
			$parent_slug,
			__( 'Next Order Coupons', 'wcnoc' ),
			__( 'Next Order Coupons', 'wcnoc' ),
			'manage_options',
			'wcnoc_options_page',
			array( $this, 'settings_page' )
		);
	}
	
	/**
	 * Styles for settings page
	 */
	public function load_scripts_styles ( $hook ) {
		if( $hook != $this->options_page_hook ) 
			return;

		// WC2.3+ load all WC scripts for product search!
		if ( version_compare( WOOCOMMERCE_VERSION, '2.3', '>=' ) ) {
			wp_enqueue_script( 'woocommerce_admin' );
			wp_enqueue_script( 'iris' );
			wp_enqueue_script( 'wc-enhanced-select' );
			wp_enqueue_script( 'jquery-ui-sortable' );
			wp_enqueue_script( 'jquery-ui-autocomplete' );

			$suffix       = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';		
			wp_enqueue_script( 'wc-admin-coupon-meta-boxes', WC()->plugin_url() . '/assets/js/admin/meta-boxes-coupon' . $suffix . '.js', array( 'wc-admin-meta-boxes', 'jquery-tiptip' ), WC_VERSION );

			$params = array(
				'plugin_url' => WC()->plugin_url(),
				'ajax_url'   => admin_url( 'admin-ajax.php' ),
			);

			wp_localize_script( 'wc-admin-meta-boxes', 'woocommerce_admin_meta_boxes', $params );
		}
	}

	/**
	 * Add settings link to plugins page
	 */
	public function wcnoc_add_settings_link( $links ) {
	    $settings_link = '<a href="admin.php?page=wcnoc_options_page">'. __( 'Settings', 'woocommerce' ) . '</a>';
	  	array_push( $links, $settings_link );
	  	return $links;
	}
	
	public function settings_page() {
		$active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'coupon';
		?>

			<div class="wrap">
				<div class="icon32" id="icon-options-general"><br /></div>
				<h2><?php _e( 'WooCommerce Next Order Coupon', 'wcnoc' ); ?></h2>
				<h2 class="nav-tab-wrapper">  
				    <a href="?page=wcnoc_options_page&tab=coupon" class="nav-tab <?php echo $active_tab == 'coupon' ? 'nav-tab-active' : ''; ?>"><?php _e('Coupon','wcnoc') ?></a>  
				    <a href="?page=wcnoc_options_page&tab=email" class="nav-tab <?php echo $active_tab == 'email' ? 'nav-tab-active' : ''; ?>"><?php _e('Email','wcnoc') ?></a>  
				</h2> 

 
				<form method="post" action="options.php">
					<?php
						switch ($active_tab) {
							case 'coupon':
								settings_fields( 'wcnoc_coupon_settings' );
								do_settings_sections( 'wcnoc_coupon_settings' );
								break;
							case 'email':
								settings_fields( 'wcnoc_email_settings' );
								do_settings_sections( 'wcnoc_email_settings' );
								break;
							default:
								settings_fields( 'wcnoc_coupon_settings' );
								do_settings_sections( 'wcnoc_coupon_settings' );
								break;
						}

						submit_button();
					?>

				</form>

			</div>

		<?php
	}
	
	/**
	 * User settings.
	 * 
	 */
	
	public function init_settings() {
		global $woocommerce;

		/**************************************/
		/*********** COUPON SETTINGS **********/
		/**************************************/

		$option = 'wcnoc_coupon_settings';
	
		// Create option in wp_options.
		if ( false == get_option( $option ) ) {
			add_option( $option );
		}
	

		// GENERAL SECTION
		add_settings_section(
			'coupon_settings',
			__( 'General coupon settings', 'wcnoc' ),
			array( &$this, 'section_options_callback' ),
			$option
		);

		add_settings_field(
			'plugin_enabled',
			__( 'Enable Next Order Coupon', 'wcnoc' ),
			array( &$this, 'checkbox_element_callback' ),
			$option,
			'coupon_settings',
			array(
				'menu'			=> $option,
				'id'			=> 'plugin_enabled',
			)
		);

		add_settings_field(
			'coupon_length',
			__( 'Coupon length (number of characters)', 'wcnoc' ),
			array( &$this, 'text_element_callback' ),
			$option,
			'coupon_settings',
			array(
				'menu'			=> $option,
				'id'			=> 'coupon_length',
				'size'			=> '5',
			)
		);

		add_settings_field(
			'discount_type',
			__( 'Discount type', 'woocommerce' ),
			array( &$this, 'select_element_callback' ),
			$option,
			'coupon_settings',
			array(
				'menu'			=> $option,
				'id'			=> 'discount_type',
				'options'		=> $this->get_coupon_discount_types(),
			)
		);
	 
		add_settings_field(
			'coupon_amount',
			__( 'Coupon amount', 'woocommerce' ),
			array( &$this, 'text_element_callback' ),
			$option,
			'coupon_settings',
			array(
				'menu'			=> $option,
				'id'			=> 'coupon_amount',
				'size'			=> '5',
				'description' => __( 'Value of the coupon.', 'woocommerce' ),
			)
		);

		// only allow 'apply before tax' setting for versions before 2.3!
		if ( version_compare( WOOCOMMERCE_VERSION, '2.3', '<' ) ) {
			add_settings_field(
				'apply_before_tax',
				__( 'Apply before tax', 'woocommerce' ),
				array( &$this, 'checkbox_element_callback' ),
				$option,
				'coupon_settings',
				array(
					'menu'			=> $option,
					'id'			=> 'apply_before_tax',
					'description'	=> __( 'Check this box if the coupon should be applied before calculating cart tax.', 'woocommerce' ),
				)
			);
		}

		add_settings_field(
			'free_shipping',
			__( 'Enable free shipping', 'woocommerce' ),
			array( &$this, 'checkbox_element_callback' ),
			$option,
			'coupon_settings',
			array(
				'menu'			=> $option,
				'id'			=> 'free_shipping',
				'description'	=> sprintf(__( 'Check this box if the coupon grants free shipping. The <a href="%s">free shipping method</a> must be enabled with the "must use coupon" setting checked.', 'woocommerce' ), admin_url('admin.php?page=woocommerce_settings&tab=shipping&section=WC_Shipping_Free_Shipping')),
			)
		);

		add_settings_field(
			'expires_in',
			__( 'Expires in (days)', 'wcnoc' ),
			array( &$this, 'text_element_callback' ),
			$option,
			'coupon_settings',
			array(
				'menu'			=> $option,
				'id'			=> 'expires_in',
				'size'			=> '5',
				'description'	=> __( 'Number of days the coupon code is valid after generation.', 'wcnoc' ),
			)
		);


		// USAGE RESTRICTION SECTION
		add_settings_section(
			'usage_restriction',
			__( 'Usage restrictions', 'wcnoc' ),
			array( &$this, 'section_options_callback' ),
			$option
		);

		add_settings_field(
			'min_order',
			__( 'Minimum order amount', 'wcnoc' ),
			array( &$this, 'text_element_callback' ),
			$option,
			'usage_restriction',
			array(
				'menu'			=> $option,
				'id'			=> 'min_order',
				'size'			=> '5',
				'description'	=> __( 'Minimum order amount threshold for coupon <b>generation</b>', 'wcnoc' ),
			)
		);	

		add_settings_field(
			'minimum_amount',
			__( 'Minimum amount', 'woocommerce' ),
			array( &$this, 'text_element_callback' ),
			$option,
			'usage_restriction',
			array(
				'menu'			=> $option,
				'id'			=> 'minimum_amount',
				'size'			=> '5',
				'description'	=> __( 'This field allows you to set the minimum subtotal needed to <b>use</b> the coupon.', 'woocommerce' ),
			)
		);

		// TODO: max spend? (which WC versions?)

		add_settings_field(
			'individual_use',
			__( 'Individual use', 'woocommerce' ),
			array( &$this, 'checkbox_element_callback' ),
			$option,
			'usage_restriction',
			array(
				'menu'			=> $option,
				'id'			=> 'individual_use',
				'description'	=> __( 'Check this box if the coupon cannot be used in conjunction with other coupons.', 'woocommerce' ),
			)
		);

		add_settings_field(
			'exclude_sale_items',
			__( 'Exclude sale items', 'woocommerce' ),
			array( &$this, 'checkbox_element_callback' ),
			$option,
			'usage_restriction',
			array(
				'menu'			=> $option,
				'id'			=> 'exclude_sale_items',
				'description'	=> __( 'Check this box if the coupon should not apply to items on sale. Per-item coupons will only work if the item is not on sale. Per-cart coupons will only work if there are no sale items in the cart.', 'woocommerce' ),
			)
		);

		// restrict product/category search for WC2.3+
		if ( version_compare( WOOCOMMERCE_VERSION, '2.3', '>=' ) ) {
			add_settings_field(
				'product_ids',
				__( 'Products', 'woocommerce' ),
				array( &$this, 'product_search_callback' ),
				$option,
				'usage_restriction',
				array(
					'menu'			=> $option,
					'id'			=> 'product_ids',
					'description'	=> __( 'Products which need to be in the cart to use this coupon or, for "Product Discounts", which products are discounted.', 'woocommerce' ),
				)
			);

			add_settings_field(
				'exclude_product_ids',
				__( 'Exclude products', 'woocommerce' ),
				array( &$this, 'product_search_callback' ),
				$option,
				'usage_restriction',
				array(
					'menu'			=> $option,
					'id'			=> 'exclude_product_ids',
					'description'	=> __( 'Products which must not be in the cart to use this coupon or, for "Product Discounts", which products are not discounted.', 'woocommerce' ),
				)
			);

			add_settings_field(
				'product_categories',
				__( 'Product categories', 'woocommerce' ),
				array( &$this, 'category_search_callback' ),
				$option,
				'usage_restriction',
				array(
					'menu'			=> $option,
					'id'			=> 'product_categories',
					'placeholder'	=> __( 'Any category', 'woocommerce' ),
					'description'	=> __( 'A product must be in this category for the coupon to remain valid or, for "Product Discounts", products in these categories will be discounted.', 'woocommerce' ),
				)
			);

			add_settings_field(
				'exclude_product_categories',
				__( 'Exclude product categories', 'woocommerce' ),
				array( &$this, 'category_search_callback' ),
				$option,
				'usage_restriction',
				array(
					'menu'			=> $option,
					'id'			=> 'exclude_product_categories',
					'placeholder'	=> __( 'No categories', 'woocommerce' ),
					'description'	=> __( 'Product must not be in this category for the coupon to remain valid or, for "Product Discounts", products in these categories will not be discounted.', 'woocommerce' ),
				)
			);
		}

		add_settings_field(
			'customer_email',
			__( 'Coupon is non-transferrable', 'wcnoc' ),
			array( &$this, 'checkbox_element_callback' ),
			$option,
			'usage_restriction',
			array(
				'menu'			=> $option,
				'id'			=> 'customer_email',
				'description'	=> __( 'Only works with the email address of the customer placed the order', 'wcnoc' ),
			)
		);

		// USAGE LIMITS SECTION
		add_settings_section(
			'usage_limits',
			__( 'Usage limits', 'wcnoc' ),
			array( &$this, 'section_options_callback' ),
			$option
		);

		add_settings_field(
			'usage_limit',
			__( 'Usage limit', 'woocommerce' ),
			array( &$this, 'text_element_callback' ),
			$option,
			'usage_limits',
			array(
				'menu'			=> $option,
				'id'			=> 'usage_limit',
				'size'			=> '5',
				'description'	=> __( 'How many times this coupon can be used before it is void.', 'woocommerce' ),
			)
		);


		// Register settings.
		register_setting( $option, $option, array( &$this, 'validate_options' ) );


		/**************************************/
		/*********** EMAIL SETTINGS **********/
		/**************************************/

		$option = 'wcnoc_email_settings';
	
		// Create option in wp_options.
		if ( false == get_option( $option ) ) {
			add_option( $option );
		}

		// Section.
		add_settings_section(
			'email_settings',
			__( 'Email settings', 'wcnoc' ),
			array( &$this, 'section_options_callback' ),
			$option
		);

		add_settings_field(
			'email_text',
			__( 'Email text', 'wcnoc' ),
			array( &$this, 'textarea_element_callback' ),
			$option,
			'email_settings',
			array(
				'menu'			=> $option,
				'id'			=> 'email_text',
				'width'			=> '60',
				'height'		=> '5',
				'description'	=> __( 'Text to be included in order confirmation emails.<br /><code>%1$s</code> = coupon code<br /><code>%2$s</code> = coupon amount/percentage', 'wcnoc' ),
			)
		);

		add_settings_field(
			'email_status',
			__( 'Email to add coupon code to', 'wcnoc' ),
			array( &$this, 'select_element_callback' ),
			$option,
			'email_settings',
			array(
				'menu'			=> $option,
				'id'			=> 'email_status',
				'options'		=> array (
					'processing'	=> __( 'Processing order', 'wcnoc' ),
					'completed'		=> __( 'Completed order', 'wcnoc' ),
				),
				'default'		=> 'completed',
			)
		);

		add_settings_field(
			'email_placement',
			__( 'Text placement', 'wcnoc' ),
			array( &$this, 'select_element_callback' ),
			$option,
			'email_settings',
			array(
				'menu'			=> $option,
				'id'			=> 'email_placement',
				'options'		=> array (
					'woocommerce_email_before_order_table'	=> __( 'Before order table', 'wcnoc' ),
					'woocommerce_email_after_order_table'	=> __( 'After order table', 'wcnoc' ),
					'woocommerce_wcnoc_text'				=> __( 'Custom location (define in template)', 'wcnoc' ),
				),
			)
		);

		// Register settings.
		register_setting( $option, $option, array( &$this, 'validate_options' ) );
	}
	
	/**
	 * Set default settings.
	 * 
	 * @return void.
	 */
	public function default_settings() {
		$default_coupon = array(
			'plugin_enabled'	=> '',
			'min_order'			=> '',
			'coupon_length'		=> '8',
			'discount_type'		=> 'fixed_cart',
			'coupon_amount'		=> '',
			'free_shipping'		=> '0',
			'individual_use'	=> '0',
			'apply_before_tax'	=> '1',
			'exclude_sale_items'=> '0',
			'minimum_amount'	=> '',
			'customer_email'	=> '1',
			'usage_limit'		=> '',
			'expires_in'		=> '',
		);

		$default_email = array(
			'email_text'		=> __( 'Use coupon code <strong>%1$s</strong> on your next visit and receive %2$s discount!', 'wcnoc' ),
			'email_placement'	=> 'woocommerce_email_before_order_table',
			'email_status'		=> 'completed',
		);

		if (!get_option( 'wcnoc_coupon_settings')) update_option( 'wcnoc_coupon_settings', $default_coupon );
		if (!get_option( 'wcnoc_email_settings')) update_option( 'wcnoc_email_settings', $default_email );
	}
	
	/**
	 * Get the types of discount available in WooCommerce
	 * @return array $discount_types
	 */
	public function get_coupon_discount_types() {
		global $woocommerce;

		if ( version_compare( WOOCOMMERCE_VERSION, '2.1' ) >= 0 ) {
			// WC 2.1 or newer is used
			$discount_types = wc_get_coupon_types();
		} else {
			// Backwards compatibility
			$discount_types = $woocommerce->get_coupon_discount_types();
		}

		$discount_types['percent_order'] = __( '% of original order (=fixed Cart Discount)', 'wcnoc' );

		return (array) $discount_types;
	}
	
	// Text element callback.
	public function text_element_callback( $args ) {
		$menu = $args['menu'];
		$id = $args['id'];
		$size = isset( $args['size'] ) ? $args['size'] : '25';
	
		$options = get_option( $menu );
	
		if ( isset( $options[$id] ) ) {
			$current = $options[$id];
		} else {
			$current = isset( $args['default'] ) ? $args['default'] : '';
		}
	
		$html = sprintf( '<input type="text" id="%1$s" name="%2$s[%1$s]" value="%3$s" size="%4$s"/>', $id, $menu, $current, $size );
	
		// Displays option description.
		if ( isset( $args['description'] ) ) {
			$html .= sprintf( '<p class="description">%s</p>', $args['description'] );
		}
	
		echo $html;
	}
	
	// Text element callback.
	public function textarea_element_callback( $args ) {
		$menu = $args['menu'];
		$id = $args['id'];
		$width = $args['width'];
		$height = $args['height'];
	
		$options = get_option( $menu );
	
		if ( isset( $options[$id] ) ) {
			$current = $options[$id];
		} else {
			$current = isset( $args['default'] ) ? $args['default'] : '';
		}
	
		$html = sprintf( '<textarea id="%1$s" name="%2$s[%1$s]" cols="%4$s" rows="%5$s"/>%3$s</textarea>', $id, $menu, $current, $width, $height );
	
		// Displays option description.
		if ( isset( $args['description'] ) ) {
			$html .= sprintf( '<p class="description">%s</p>', $args['description'] );
		}
	
		echo $html;
	}


	/**
	 * Checkbox field callback.
	 *
	 * @param  array $args Field arguments.
	 *
	 * @return string	  Checkbox field.
	 */
	public function checkbox_element_callback( $args ) {
		$menu = $args['menu'];
		$id = $args['id'];
	
		$options = get_option( $menu );
	
		if ( isset( $options[$id] ) ) {
			$current = $options[$id];
		} else {
			$current = isset( $args['default'] ) ? $args['default'] : '';
		}
	
		$html = sprintf( '<input type="checkbox" id="%1$s" name="%2$s[%1$s]" value="1"%3$s />', $id, $menu, checked( 1, $current, false ) );
	
		//$html .= sprintf( '<label for="%s"> %s</label><br />', $id, __( 'Activate/Deactivate', 'wcnoc' ) );
	
		// Displays option description.
		if ( isset( $args['description'] ) ) {
			$html .= sprintf( '<p class="description">%s</p>', $args['description'] );
		}
	
		echo $html;
	}
	
	/**
	 * Select element callback.
	 *
	 * @param  array $args Field arguments.
	 *
	 * @return string	  Select field.
	 */
	public function select_element_callback( $args ) {
		$menu = $args['menu'];
		$id = $args['id'];
	
		$options = get_option( $menu );
	
		if ( isset( $options[$id] ) ) {
			$current = $options[$id];
		} else {
			$current = isset( $args['default'] ) ? $args['default'] : '';
		}
	
		$html = sprintf( '<select id="%1$s" name="%2$s[%1$s]">', $id, $menu );

		foreach ( $args['options'] as $key => $label ) {
			$html .= sprintf( '<option value="%s"%s>%s</option>', $key, selected( $current, $key, false ), $label );
		}

		$html .= '</select>';
	
		// Displays option description.
		if ( isset( $args['description'] ) ) {
			$html .= sprintf( '<p class="description">%s</p>', $args['description'] );
		}
	
		echo $html;
	}

	// Product search callback.
	public function product_search_callback( $args ) {
		$menu = $args['menu'];
		$id = $args['id'];
		$size = isset( $args['size'] ) ? $args['size'] : '25';
	
		$options = get_option( $menu );
	
		if ( isset( $options[$id] ) ) {
			$current = $options[$id];
		} else {
			$current = isset( $args['default'] ) ? $args['default'] : '';
		}
	
		$input_name = "{$menu}[{$id}]";

		?>
		<input type="hidden" class="wc-product-search" data-multiple="true" style="width: 50%;" name="<?php echo $input_name; ?>" data-placeholder="<?php _e( 'Search for a product&hellip;', 'woocommerce' ); ?>" data-action="woocommerce_json_search_products_and_variations" data-selected="<?php
			$product_ids = array_filter( array_map( 'absint', explode( ',', $current ) ) );
			$json_ids    = array();

			foreach ( $product_ids as $product_id ) {
				$product = wc_get_product( $product_id );
				$json_ids[ $product_id ] = wp_kses_post( $product->get_formatted_name() );
			}

			echo esc_attr( json_encode( $json_ids ) );
		?>" value="<?php echo implode( ',', array_keys( $json_ids ) ); ?>" />
		<?php
		// Displays option description.
		if ( isset( $args['description'] ) ) {
			printf( '<p class="description">%s</p>', $args['description'] );
		}
	}

	// Category search callback.
	public function category_search_callback( $args ) {
		$menu = $args['menu'];
		$id = $args['id'];
		$size = isset( $args['size'] ) ? $args['size'] : '25';
	
		$options = get_option( $menu );
	
		if ( isset( $options[$id] ) ) {
			$current = $options[$id];
		} else {
			$current = isset( $args['default'] ) ? $args['default'] : '';
		}
	
		$input_name = "{$menu}[{$id}]";

		?>
		<select id="<?php echo $input_name; ?>" name="<?php echo $input_name; ?>[]" style="width: 50%;"  class="wc-enhanced-select" multiple="multiple" data-placeholder="<?php echo $args['placeholder']; ?>">
			<?php
				$category_ids = (array) $current;
				$categories   = get_terms( 'product_cat', 'orderby=name&hide_empty=0' );

				if ( $categories ) foreach ( $categories as $cat ) {
					echo '<option value="' . esc_attr( $cat->term_id ) . '"' . selected( in_array( $cat->term_id, $category_ids ), true, false ) . '>' . esc_html( $cat->name ) . '</option>';
				}
			?>
		</select>
		<?php
		// Displays option description.
		if ( isset( $args['description'] ) ) {
			printf( '<p class="description">%s</p>', $args['description'] );
		}
	}

	/**
	 * Section null callback.
	 *
	 * @return void.
	 */
	public function section_options_callback() {
	
	}
	
	/**
	 * Validate options.
	 *
	 * @param  array $input options to valid.
	 *
	 * @return array		validated options.
	 */
	public function validate_options( $input ) {
		return $input;
	}

}