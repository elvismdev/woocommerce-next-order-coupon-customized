<?php
/*
Plugin Name: WooCommerce Next Order Coupon
Plugin URI: https://wpovernight.com/downloads/woocommerce-next-order-coupon/
Description: Generates a unique WooCommerce coupon code for every completed order and adds it to the order confirmation email.
Author: Ewout Fernhout
Author URI: www.wpovernight.com
Version: 1.4.1
Text Domain: wcnoc

License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

if ( !class_exists( 'WooCommerce_Next_Order_Coupon' ) ) {
	class WooCommerce_Next_Order_Coupon {
	
		public static $plugin_basename;

		/**
		 * Construct.
		 */
				
		public function __construct() {
			self::$plugin_basename = plugin_basename(__FILE__);

			$this->coupon_options = get_option('wcnoc_coupon_settings');
			$this->email_options = get_option('wcnoc_email_settings');

			add_action( 'init', array( &$this, 'init' ) );
			
			// Init updater data
			$this->item_name	= 'WooCommerce Next Order Coupon';
			$this->file			= __FILE__;
			$this->license_slug	= 'wcnoc_license';
			$this->version		= '1.4.1';
			$this->author		= 'Ewout Fernhout';

			add_action( 'init', array( &$this, 'load_updater' ), 0 );
		}

		/**
		 * Load the main plugin classes and functions
		 */
		public function init () {
			include_once( 'includes/wcnoc-settings.php' );
			$this->settings = new WC_Next_Order_Coupon_Settings();

			// set trigger status
			$status_setting = isset($this->email_options['email_status']) ? $this->email_options['email_status'] : 'completed';
			$this->trigger_status = apply_filters( 'woocommerce_wcnoc_status', $status_setting );

			//  Generate coupon at user defined status (complete or processing via settings, or other status via filter)
			add_action( 'woocommerce_order_status_'.$this->trigger_status, array( &$this, 'generate_next_order_coupon' ) );

			// Generate coupon when order is marked as processing too
			add_action( 'woocommerce_order_status_processing', array( &$this, 'generate_next_order_coupon' ) );

			// Display coupon code on completed order page
			// add_action( 'woocommerce_thankyou', array( $this, 'thankyou_coupon_code' ), 10 );
			
			// Add coupon code to customer emails
			// woocommerce_email_before_order_table / woocommerce_email_after_order_table / woocommerce_wconc_text
			if (isset($this->email_options['email_placement'])) {
				add_action( $this->email_options['email_placement'], array( &$this, 'email_coupon_code' ), 10, 2 );
			}
		}

		/**
		 * Display coupon code on completed order page
		 */
		public function thankyou_coupon_code() {
			//not used for now
		}
	
		/**
		* Add coupon code to customer emails
		**/
		public function email_coupon_code( $order, $sent_to_admin ) {
			// if ( $sent_to_admin ) return; // only send to customers

			if ( $order->status != $this->trigger_status ) return;		

			echo $this->get_email_text($order);
		}
	
		/**
		* Assemble text for email
		**/
		public function get_email_text($order) {
			$coupon_code = get_post_meta($order->id,'_wcnoc',true);
			$coupon_id = get_post_meta($order->id,'_wcnoc_id',true);

			if ( empty($coupon_code) && empty($coupon_id) ) {
				return;
			}
						
			$coupon_meta = get_post_meta( $coupon_id );	
			$coupon_discount_type = $coupon_meta['discount_type'][0];
			$coupon_amount = $coupon_meta['coupon_amount'][0];
			
			if ($coupon_discount_type == 'percent' || $coupon_discount_type == 'percent_product') {
				$coupon_discount = $coupon_amount . '%';
			} else {
				$coupon_discount = woocommerce_price($coupon_amount);
			}
			
			$email_text_setting = $this->email_options['email_text'];
			$email_text = sprintf( $email_text_setting, $coupon_code, $coupon_discount );
			$email_text = apply_filters( 'wcnoc_email_text', $email_text, $coupon_code, $coupon_discount, $order->id );
			
			$email_text = '<p>' . $email_text . '</p>';
			
			return $email_text;
		}
		
		/**
		 * Generate coupon
		 */
		function generate_next_order_coupon ( $order_id ) {
			// check if next order coupon already exists
			$wcnoc = get_post_meta( $order_id, '_wcnoc', true );
			if ( !empty($wcnoc) ) {
				return;
			}

			// check if plugin is enabled in settings
			if ( empty($this->coupon_options['plugin_enabled']) ) {
				return; 
			}

			if ( !( apply_filters('wcnoc_custom_condition', true, $order_id ) ) ) {
				// use this filter to add an extra condition - return false to disable the coupon creation
				return;
			}

			$order_meta = get_post_meta( $order_id );
			$customer_email = $order_meta['_billing_email'][0];
			$order_amount = $order_meta['_order_total'][0];
			
			if ( !empty($this->coupon_options['expires_in']) ) {
				$expiry_date = date("Y-m-d", strtotime("+ " . $this->coupon_options['expires_in'] . " days")); //add number of days to current date
			} else {
				$expiry_date = '';
			}
			
			if ( $order_amount < $this->coupon_options['min_order'] ) return; //minimum order requirement not met
			
			// calculate discount if percent_order type used
			if ($this->coupon_options['discount_type'] == 'percent_order') {
				$this->coupon_options['discount_type'] = 'fixed_cart';
				$percent = trim(str_replace('%', '', $this->coupon_options['coupon_amount']));
				$this->coupon_options['coupon_amount'] = round( ($order_amount * ( $percent / 100 )), 2);
			}

			// Create coupon
			$coupon_code = apply_filters( 'wcnoc_coupon_code', $this->randomstring($this->coupon_options['coupon_length']), $order_id, $order_meta );
			$coupon_meta = array(
				'discount_type'					=> $this->coupon_options['discount_type'],
				'coupon_amount'					=> $this->coupon_options['coupon_amount'],
				'individual_use'				=> (isset($this->coupon_options['individual_use']) && $this->coupon_options['individual_use']=='1')?'yes':'no',
				'product_ids'					=> isset($this->coupon_options['product_ids']) ? $this->coupon_options['product_ids'] : '',
				'exclude_product_ids'			=> isset($this->coupon_options['exclude_product_ids']) ? $this->coupon_options['exclude_product_ids'] : '',
				'product_categories'			=> isset($this->coupon_options['product_categories']) ? $this->coupon_options['product_categories'] : '',
				'exclude_product_categories'	=> isset($this->coupon_options['exclude_product_categories']) ? $this->coupon_options['exclude_product_categories'] : '',
				'exclude_sale_items'			=> (isset($this->coupon_options['exclude_sale_items']) && $this->coupon_options['exclude_sale_items']=='1')?'yes':'no',
				'usage_limit'					=> $this->coupon_options['usage_limit'],
				'expiry_date'					=> $expiry_date,
				'apply_before_tax'				=> (isset($this->coupon_options['apply_before_tax']) && $this->coupon_options['apply_before_tax']=='1')?'yes':'no',
				'free_shipping'					=> (isset($this->coupon_options['free_shipping']) && $this->coupon_options['free_shipping']=='1')?'yes':'no',
				'customer_email'				=> (isset($this->coupon_options['customer_email']) && $this->coupon_options['customer_email']=='1')? $customer_email:'',
				'minimum_amount'				=> $this->coupon_options['minimum_amount'],
			);
			$coupon_id = $this->create_coupon( $coupon_code, $coupon_meta, $order_id );

			if ( ! update_post_meta ( $order_id, '_wcnoc', $coupon_code ) ) add_post_meta( $order_id, '_wcnoc', $coupon_code );
			if ( ! update_post_meta ( $order_id, '_wcnoc_id', $coupon_id ) ) add_post_meta( $order_id, '_wcnoc_id', $coupon_id );
		}

		/**
		 * Create a coupon programmatically
		 */
		public function create_coupon ( $coupon_code, $coupon_meta, $order_id = '' ) {
			$coupon = array(
				'post_title' => $coupon_code,
				'post_content'	=> '',
				'post_status'	=> 'publish',
				'post_author'	=> 1,
				'post_type'		=> 'shop_coupon'
			);
			
			// Temporary remove meta action
			if ( version_compare( WOOCOMMERCE_VERSION, '2.1', '<=' ) ) {
				// pre-2.1: use old action
				$function_to_add = 'woocommerce_process_shop_coupon_meta';
				$priority = 1;
			} else {
				$function_to_add = 'WC_Meta_Box_Coupon_Data::save';
				$priority = 10;
			}
			
			remove_action( 'woocommerce_process_shop_coupon_meta', $function_to_add, $priority );

			// Create coupon post
			$coupon_id = wp_insert_post( $coupon );

			// Re-activate meta action
			add_action( 'woocommerce_process_shop_coupon_meta', $function_to_add, $priority, 2 );


			// apply before tax setting deprecated in wc2.3
			if ( version_compare( WOOCOMMERCE_VERSION, '2.3', '>=' ) ) {
				unset($coupon_meta['apply_before_tax']);
			}

			// Add coupon meta
			$coupon_meta = apply_filters( 'wcnoc_coupon_meta', $coupon_meta, $coupon_id, $order_id );
			foreach ($coupon_meta as $meta_key => $meta_value) {
				update_post_meta( $coupon_id, $meta_key, $meta_value );
			}

			return $coupon_id;		
		}

		/**
		 * Generate random string
		 */
		public function randomstring($length = 8) {
			$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
			//$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$randomstring = '';
			for ($i = 0; $i < $length; $i++) {
				$randomstring .= $characters[rand(0, strlen($characters) - 1)];
			}
			return $randomstring;
		}

		/**
		 * Parse coupon url & apply to cart
		 * Thanks to AudioTheme! https://github.com/AudioTheme/woocommerce-coupon-links/
		 * 
		 * NOT CURRENTLY IN USE! Difficult with several coupon settings...
		 * 
		 */
		public function woocommerce_coupon_url() {
			global $woocommerce;
			
			// Bail early if $woocommerce is empty or coupons are not enabled in WooCommerce
			if ( empty( $woocommerce ) ) {
				return;
			}
			
			// Get coupon query variable
			$coupon_var = apply_filters( 'woocommerce_coupon_links_query_var', 'coupon_code' );
			
			// Set coupon code
			$coupon_code = isset( $_GET[ $coupon_var ] ) ? $_GET[ $coupon_var ] : '';
			
			// Bail early if coupon code has not been set or has already been applied
			if ( empty( $coupon_code ) || in_array( $coupon_code, $woocommerce->cart->applied_coupons ) )
				return;
			
			// Apply coupon code to cart
			$woocommerce->cart->add_discount( sanitize_text_field( $coupon_code ) );
		}

		/**
		 * Run the updater scripts from the Sidekick
		 * @return void
		 */
		public function load_updater() {
			// Check if sidekick is loaded
			if (class_exists('WPO_Updater')) {
				$this->updater = new WPO_Updater( $this->item_name, $this->file, $this->license_slug, $this->version, $this->author );
			}
		}

	}
}


// Load translations & Register defaults on activation
load_plugin_textdomain( 'wcnoc', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
include_once( 'includes/wcnoc-settings.php' );
register_activation_hook( __FILE__, array( 'WC_Next_Order_Coupon_Settings', 'default_settings' ) );


/**
 * WooCommerce fallback notice.
 *
 * @return string Fallack notice.
 */
function wcnoc_fallback_notice() {
	$message = '<div class="error">';
		$message .= '<p>' . sprintf( __( 'WooCommerce Next Order Coupon requires <a href="%s">WooCommerce</a> to be installed & activated!' , 'wcnoc' ), 'http://wordpress.org/extend/plugins/woocommerce/' ) . '</p>';
	$message .= '</div>';

	echo $message;
}

/**
 * Check if WooCommerce is active.
 */
add_action( 'plugins_loaded', 'wcnoc_init' );
function wcnoc_init(){
	global $wcnoc;
	if( class_exists('woocommerce') ) {
		$wcnoc = new WooCommerce_Next_Order_Coupon();
	} else {
		add_action( 'admin_notices', 'wcnoc_fallback_notice' );
	}
}

/**
 * WPOvernight updater admin notice
 */
if ( ! class_exists( 'WPO_Updater' ) && ! function_exists( 'wpo_updater_notice' ) ) {

	if ( ! empty( $_GET['hide_wpo_updater_notice'] ) ) {
		update_option( 'wpo_updater_notice', 'hide' );
	}

	/**
	 * Display a notice if the "WP Overnight Sidekick" plugin hasn't been installed.
	 * @return void
	 */
	function wpo_updater_notice() {
		$wpo_updater_notice = get_option( 'wpo_updater_notice' );

		$blog_plugins = get_option( 'active_plugins', array() );
		$site_plugins = get_site_option( 'active_sitewide_plugins', array() );
		$plugin = 'wpovernight-sidekick/wpovernight-sidekick.php';

		if ( in_array( $plugin, $blog_plugins ) || isset( $site_plugins[$plugin] ) || $wpo_updater_notice == 'hide' ) {
			return;
		}

		echo '<div class="updated fade"><p>Install the <strong>WP Overnight Sidekick</strong> plugin to receive updates for your WP Overnight plugins - check your order confirmation email for more information. <a href="'.add_query_arg( 'hide_wpo_updater_notice', 'true' ).'">Hide this notice</a></p></div>' . "\n";
	}

	add_action( 'admin_notices', 'wpo_updater_notice' );
}
