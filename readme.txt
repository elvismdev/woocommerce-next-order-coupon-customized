=== Plugin Name ===
Contributors: pomegranate
Tags: woocommerce, coupons, email
Requires at least: 3.7.1
Tested up to: 4.2
Stable tag: 1.4.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Generates a unique WooCommerce coupon code for every completed order and adds it to the order confirmation email.

== Description ==

This plugin helps you to increase sales by stimulating customers to make their next order in your shop. For every
completed order, a coupon code is generated and sent to the customer in the order confirmation email.

Main features:
* Includes all WooCommerce coupon settings for the default values of the Next Order Coupon (expiration, number of uses, min. order amount, etc.)
* Lets you define a minimum order amount as a threshhold for coupon generation
* Easily edit & format email text and location
* Includes actions/filters to be used for your email templates for even greater flexibility

== Installation ==

1. Upload woocommerce-next-order-coupon.zip via the WordPress plugin interface & activate
1. Go to WooCommerce > Next Order Coupon and set all the default values for the coupon to be used
1. Edit the email text (you can include things like 'This coupon is only valid for 2 weeks!' if you have put this in the settings)
1. Enable coupon generation by ticking the box on the top of the settings panel and you're all set!

== Frequently Asked Questions ==

= How can I change the color of the coupon code in the email? =

You can use HTML in your email text, and this also includes inline styles (CSS).
1. Go to *WooCommerce > Next Order Coupons > Email*
1. Replace `<strong>%1$s</strong>` with `<strong style="color:red;">%1$s</strong>`. Note that the color should be an [HTML color](http://www.w3schools.com/html/html_colornames.asp) or color code.

= How can I put the email text in a custom location/template? =

WooCommerce Next Order Coupon includes an action that allows you to put the text anywhere you want.
Simply put `<?php do_action('woocommerce_wcnoc_text', $order, false); ?>` in your email template and
select 'Custom location' in the plugins email settings panel. The plugin will then output the text to this action.

== Screenshots ==


== Changelog ==

= 1.4.1 =
* Fix: Settings page error for WC2.0

= 1.4.0 =
* Exclude/include specific products & categories (WooCommerce 2.3+ only)

= 1.3.3 =
* Pass order ID to email text filter

= 1.3.2 =
* Feature: Calculate coupon amount based on the original order
* Feature: Filter for finer control over coupon options ('wcnoc_coupon_meta')

= 1.3.1 =
* Compatibility: WooCommerce 2.3 compatibility (no more 'apply before tax' setting in coupons)
* Feature: filter for coupon code ('wcnoc_coupon_code') to allow coupon code customization
* Feature: WPML compatibility (for translating the email text)
* Tweak: Option to hide WP Overnight Sidekick notice

= 1.3.0 =
* Feature: filter for custom condition for coupon creation ('wcnoc_custom_condition')
* Fix: Errors when changing the order status to complete on the edit order screen (latest version of WooCommerce)

= 1.2.0 =
* WPO updater integration

= 1.1.0 =
* Code cleanup

= 1.0.0 =
* First release